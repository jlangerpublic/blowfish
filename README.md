Usage:
```php
use JLanger\Blowfish\Blowfish;

require_once __DIR__ . '/../vendor/autoload.php';

$key = 'hgjdjHJkdjHGJ7937dHJ93nj';
$blowfish = new Blowfish($key);

echo 'encrypt string "Hello World"<br>';
$stringToEncrypt = 'Hello World';
$encryptedString = $blowfish->encrypt($stringToEncrypt);
echo 'Encrypted String: ' . $encryptedString . '<br>';
echo 'decrypt encrypted string:' . '<br>';
echo $blowfish->decrypt($encryptedString);
```