<?php
declare(strict_types=1);

namespace JLanger\Blowfish;

use function strlen;

class Blowfish
{
    /** @var XTEA */
    private XTEA $xtea;

    /**
     * Blowfish constructor.
     *
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->xtea = new XTEA($key);
    }

    /**
     * decrypt encrypted string.
     * @param string $string
     *
     * @return string
     */
    public function decrypt(string $string): ?string 
    {
        return strlen($string) > 0 
            ? $this->xtea->decrypt($string)
            : $string;
    }

    /**
     * encrypt a specified plain string.
     * @param string $string
     *
     * @return string
     */
    public function encrypt(string $string): string
    {
        return strlen($string) > 0
            ? $this->xtea->encrypt($string)
            : $string;
    }
}